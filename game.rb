require_relative "player.rb"
require "set"

class Game
	attr_accessor :board, :player_1, :player_2
	def initialize
		@cells = {
			1 => "-", 2 => "-", 3 => "-", 4 => "-", 5 => "-",
			6 => "-", 7 => "-", 8 => "-", 9 => "-" }
		@turns = 9
		@current_turn = 0
		@player_1 = Player.new
		@player_2 = Player.new
		@player_1.mark = "X"
		@player_2.mark = "O"
		@winning_combos = Set.new([Set[1, 2, 3], Set[4,5,6], Set[7,8,9], Set[1,4,7], Set[2,5,8], Set[3,6,9], Set[1,5,9], Set[3,5,7]])
		@player_1_score = Set.new
		@player_2_score = Set.new
		@arr = Set.new
		@win = false
		info
	end
	def info
		print "Player 1's mark is #{@player_1.mark}. Now please enter your name: "
		input1 = gets.chomp
		print "Player 1's mark is #{@player_2.mark}. Now please enter your name: "
		input2 = gets.chomp
		@player_1.name = input1.capitalize.strip
		@player_2.name = input2.capitalize.strip
		puts "Welcome #{@player_1.name} and #{@player_2.name}"
		board
	end
	def board
		puts "#{@cells[1]} | #{@cells[2]} | #{@cells[3]}"
		puts "#{@cells[4]} | #{@cells[5]} | #{@cells[6]}"
		puts "#{@cells[7]} | #{@cells[8]} | #{@cells[9]}"
		logic
		#check_win
	end
	def logic
	   until @win
	   		game
	   	end
	end
	def change_turns
		@player = nil
		if @current_turn % 2 == 0
			@arr = @player_1_score
			@player = @player_2.mark
		else
			@player = @player_1.mark
			@arr = @player_2_score
		end
	end
	def check_win
	@winning_combos.each do |f|
			if f.subset?(@arr)
				@win = true
			end
		end
	if @win
		puts "-----------------------"
		puts "Winner is #{@player}"
		puts "-----------------------"
		return true
		quit!
	elsif !@win && @current_turn == 9
		puts "No winners"
		exit!
		end
	end
	def game
		print "Choose a number from 1-9: "
		puts "Current turn: #{@current_turn}"
		user_input = gets.chomp
		cell = user_input.strip.to_i
		if @cells[cell] == "-"
			@current_turn += 1
			change_turns
			@arr << cell
			@cells[cell] = @player
			check_win
			else
			 puts "This cell is occupied"
			 return false
		end
		board
	end

end
game = Game.new
game