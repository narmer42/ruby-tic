require_relative "../player"

describe "Test the player"  do
	it "Check that the player's name is writable" do
		player = Player.new
		player.name = "Bane"
		expect(player.name).to eql("Bane")
		player2 = Player.new
		player2.name = "Bane2"
		expect(player2.name).to eql("Bane2")
	end
	
end
